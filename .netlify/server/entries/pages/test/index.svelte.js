var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __reExport = (target, module2, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  return __reExport(__markAsModule(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", module2 && module2.__esModule && "default" in module2 ? { get: () => module2.default, enumerable: true } : { value: module2, enumerable: true })), module2);
};
__export(exports, {
  default: () => Test
});
var import_index_1e54ea6c = __toModule(require("../../../chunks/index-1e54ea6c.js"));
var import_podcastPlayer_7666757f = __toModule(require("../../../chunks/podcastPlayer-7666757f.js"));
const Test = (0, import_index_1e54ea6c.c)(($$result, $$props, $$bindings, slots) => {
  return `<div class="${"grid h-screen"}"><div><h1 class="${"text-4xl m-8 text-center"}">Welcome to Curry &amp; The Keeper</h1>
		<h2 class="${"text-2xl m-8 text-center"}">Uniquely American, unapologetically in love</h2></div>
	<div class="${"grid grid-cols-3 "}"><div class="${"justify-self-center grid text-center self-center"}"><h3 class="${"self-center text-xl"}">Tina and Adam Curry Discuss Life, Love and Wine.</h3>
			<div class="${"justify-self-center grid"}"><img class="${"rounded-2xl w-1/2 p-2 h-auto justify-self-center"}" src="${"christmas.jpeg"}" alt="${"adam and tina in front of a christmas tree"}"></div></div>
		<div class="${"m-10 col-span-2"}">${(0, import_index_1e54ea6c.v)(import_podcastPlayer_7666757f.P, "PodcastPlayer").$$render($$result, {}, {}, {})}</div></div>
	<div class="${"text-center m-8 justify-self-center grid"}"><h3 class="${"m-4 text-xl"}">Donate to the Hill Country</h3>
		<img src="${"catkbitcoin.png"}" alt="${""}"></div>
	</div>`;
});
