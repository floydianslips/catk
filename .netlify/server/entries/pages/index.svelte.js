var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __reExport = (target, module2, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  return __reExport(__markAsModule(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", module2 && module2.__esModule && "default" in module2 ? { get: () => module2.default, enumerable: true } : { value: module2, enumerable: true })), module2);
};
__export(exports, {
  default: () => Routes
});
var import_index_1e54ea6c = __toModule(require("../../chunks/index-1e54ea6c.js"));
var import_podcastPlayer_7666757f = __toModule(require("../../chunks/podcastPlayer-7666757f.js"));
const Profile = (0, import_index_1e54ea6c.c)(($$result, $$props, $$bindings, slots) => {
  return `<div id="${"profile"}" class="${"w-full lg:w-3/5 rounded-lg lg:rounded-l-lg lg:rounded-r-none shadow-2xl bg-white opacity-90 mx-6 lg:mx-0 m-8"}"><div class="${"p-4 md:p-12 text-center lg:text-left"}">
		<div class="${"self-start block lg:hidden rounded-full shadow-xl mx-auto -mt-16 h-48 w-48 bg-cover bg-center"}" style="${"background-image: url('../static/christmas.jpeg')"}"></div>
		<img class="${"w-1/2"}" src="${"catkbitcoin.png"}" alt="${""}"></div></div>


<div class="${"w-full lg:w-2/5"}">
	<img src="${"christmas.jpeg"}" class="${"rounded-none lg:rounded-lg shadow-2xl hidden lg:block"}" alt="${"christmas time"}"></div>`;
});
var body_svelte_svelte_type_style_lang = "";
const css = {
  code: ".rss.svelte-13risu9{display:flex;justify-content:center;width:100%}.feed.svelte-13risu9{background:#0c1824;opacity:80%;color:#f7f8f9;padding:0rem 0.75rem 0.1rem 0.75rem;text-align:center;align-content:center}",
  map: null
};
const Body = (0, import_index_1e54ea6c.c)(($$result, $$props, $$bindings, slots) => {
  $$result.css.add(css);
  return `<div class="${"max-w-4xl flex items-center h-auto flex-wrap mx-auto my-32 lg:my-0"}">
	${(0, import_index_1e54ea6c.v)(Profile, "Profile").$$render($$result, {}, {}, {})}
	<div class="${"contents"}">${(0, import_index_1e54ea6c.v)(import_podcastPlayer_7666757f.P, "PodcastPlayer").$$render($$result, {}, {}, {})}</div>
	<div class="${"mb-10 rss  svelte-13risu9"}"><h4 class="${"text-lg rounded-lg underline feed svelte-13risu9"}"><a href="${"http://feed.nashownotes.com/catkrss.xml"}">Podcast Feed</a></h4></div>
</div>`;
});
const Routes = (0, import_index_1e54ea6c.c)(($$result, $$props, $$bindings, slots) => {
  return `<head><meta charset="${"UTF-8"}">
	<meta name="${"viewport"}" content="${"width=device-width, initial-scale=1.0"}">
	<meta http-equiv="${"X-UA-Compatible"}" content="${"ie=edge"}">
	<title>Curry and the Keeper</title>
	<meta name="${"description"}" content="${""}">
	<meta name="${"keywords"}" content="${""}">
	<meta name="${"author"}" content="${""}">

	
	<link rel="${"stylesheet"}" href="${"https://use.fontawesome.com/releases/v5.3.1/css/all.css"}">

	<link rel="${"stylesheet"}" href="${"https://unpkg.com/tailwindcss@2.2.19/dist/tailwind.min.css"}">
	</head>

<body class="${"font-sans antialiased text-gray-900 leading-normal tracking-wider bg-cover"}" style="${"background-image:url('https://upload.wikimedia.org/wikipedia/commons/b/b3/Wineries_Hwy_99.jpg');"}">${(0, import_index_1e54ea6c.v)(Body, "Body").$$render($$result, {}, {}, {})}

	<script src="${"https://unpkg.com/popper.js@1/dist/umd/popper.min.js"}"><\/script>
	<script src="${"https://unpkg.com/tippy.js@4"}"><\/script>
	<script>//Init tooltips
		tippy('.link', {
			placement: 'bottom'
		});

		//Toggle mode
		const toggle = document.querySelector('.js-change-theme');
		const body = document.querySelector('body');
		const profile = document.getElementById('profile');
	<\/script></body>`;
});
