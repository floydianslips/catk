const { init } = require('../handler.js');

exports.handler = init({
	appDir: "_app",
	assets: new Set(["catk-black.svg","catk.svg","catkbitcoin.png","christmas.jpeg","favicon.png","logo.png"]),
	_: {
		mime: {".svg":"image/svg+xml",".png":"image/png",".jpeg":"image/jpeg"},
		entry: {"file":"start-e4dc189f.js","js":["start-e4dc189f.js","chunks/vendor-3c871509.js"],"css":["assets/start-d5b4de3e.css"]},
		nodes: [
			() => Promise.resolve().then(() => require('../server/nodes/0.js')),
			() => Promise.resolve().then(() => require('../server/nodes/1.js')),
			() => Promise.resolve().then(() => require('../server/nodes/2.js')),
			() => Promise.resolve().then(() => require('../server/nodes/3.js'))
		],
		routes: [
			{
				type: 'page',
				pattern: /^\/$/,
				params: null,
				path: "/",
				a: [0,2],
				b: [1]
			},
			{
				type: 'page',
				pattern: /^\/test\/?$/,
				params: null,
				path: "/test",
				a: [0,3],
				b: [1]
			}
		]
	}
});
